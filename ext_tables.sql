#
# Table structure for table 'tx_kitt3nmap_domain_model_map'
#
CREATE TABLE tx_kitt3nmap_domain_model_map (

	name varchar(255) DEFAULT '' NOT NULL,
	stylefile int(11) unsigned NOT NULL default '0',
	width varchar(255) DEFAULT '' NOT NULL,
	widthunit int(11) DEFAULT '0' NOT NULL,
	height varchar(255) DEFAULT '' NOT NULL,
	heightunit int(11) DEFAULT '0' NOT NULL,
	fitbounds smallint(5) unsigned DEFAULT '0' NOT NULL,
	lat varchar(255) DEFAULT '' NOT NULL,
	lon varchar(255) DEFAULT '' NOT NULL,
	zoom int(11) DEFAULT '0' NOT NULL,
	minzoom int(11) DEFAULT '0' NOT NULL,
	maxzoom int(11) DEFAULT '0' NOT NULL,
	language varchar(255) DEFAULT '' NOT NULL,
	type varchar(255) DEFAULT '' NOT NULL,
	tilt int(11) DEFAULT '0' NOT NULL,
	controls int(11) DEFAULT '0' NOT NULL,
	controlsize varchar(255) DEFAULT '' NOT NULL,
	controlzoom smallint(5) unsigned DEFAULT '0' NOT NULL,
	controlzoompos varchar(255) DEFAULT '' NOT NULL,
	controltype smallint(5) unsigned DEFAULT '0' NOT NULL,
	controltypepos varchar(255) DEFAULT '' NOT NULL,
	controltypestyle varchar(255) DEFAULT '' NOT NULL,
	controlstreetview smallint(5) unsigned DEFAULT '0' NOT NULL,
	controlstreetviewpos varchar(255) DEFAULT '' NOT NULL,
	controlrotate smallint(5) unsigned DEFAULT '0' NOT NULL,
	controlrotatepos varchar(255) DEFAULT '' NOT NULL,
	controlscale smallint(5) unsigned DEFAULT '0' NOT NULL,
	controlfullscreen smallint(5) unsigned DEFAULT '0' NOT NULL,
	controlfullscreenpos varchar(255) DEFAULT '' NOT NULL,
	panning smallint(5) unsigned DEFAULT '0' NOT NULL,
	keyboardshortcuts smallint(5) unsigned DEFAULT '0' NOT NULL,
	doubleclickzoom smallint(5) unsigned DEFAULT '0' NOT NULL,
	scrollzoom smallint(5) unsigned DEFAULT '0' NOT NULL,
	clickablepoints smallint(5) unsigned DEFAULT '0' NOT NULL,
	mc smallint(5) unsigned DEFAULT '0' NOT NULL,
	mctype int(11) DEFAULT '0' NOT NULL,
	mcgridsize int(11) DEFAULT '0' NOT NULL,
	mcmaxzoom int(11) DEFAULT '0' NOT NULL,
	mczoomonclick smallint(5) unsigned DEFAULT '0' NOT NULL,
	mcaveragecenter smallint(5) unsigned DEFAULT '0' NOT NULL,
	mcminclustersize int(11) DEFAULT '0' NOT NULL,
	mcstyles text,
	item int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nmap_domain_model_item'
#
CREATE TABLE tx_kitt3nmap_domain_model_item (

	type int(11) DEFAULT '0' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	lat varchar(255) DEFAULT '' NOT NULL,
	lon varchar(255) DEFAULT '' NOT NULL,
	icon varchar(255) DEFAULT '' NOT NULL,
	icondefaults int(11) DEFAULT '0' NOT NULL,
	iconcolor varchar(255) DEFAULT '' NOT NULL,
	iconwidth int(11) DEFAULT '0' NOT NULL,
	iconheight int(11) DEFAULT '0' NOT NULL,
	iconoriginx int(11) DEFAULT '0' NOT NULL,
	iconoriginy int(11) DEFAULT '0' NOT NULL,
	iconfile int(11) unsigned NOT NULL default '0',
	paths int(11) unsigned NOT NULL default '0',
	strokecolor varchar(255) DEFAULT '' NOT NULL,
	strokeopacity varchar(255) DEFAULT '' NOT NULL,
	strokewidth int(11) DEFAULT '0' NOT NULL,
	fillcolor varchar(255) DEFAULT '' NOT NULL,
	fillopacity varchar(255) DEFAULT '' NOT NULL,
	scale double(11,2) DEFAULT '0.00' NOT NULL,
	window text,
	windowimage int(11) unsigned NOT NULL default '0',
	windowstate smallint(5) unsigned DEFAULT '0' NOT NULL,
	animationtype varchar(255) DEFAULT '' NOT NULL,
	animationdelay int(11) DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nmap_domain_model_map'
#
CREATE TABLE tx_kitt3nmap_domain_model_map (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_kitt3nmap_map_item_mm'
#
CREATE TABLE tx_kitt3nmap_map_item_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_kitt3nmap_domain_model_item'
#
CREATE TABLE tx_kitt3nmap_domain_model_item (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
#
# Table structure for table 'tx_kitt3nmap_domain_model_map'
#
CREATE TABLE tx_kitt3nmap_domain_model_map (
    type varchar(255) DEFAULT '' NOT NULL,
	language varchar(255) DEFAULT '' NOT NULL,
	controlzoompos varchar(255) DEFAULT '' NOT NULL,
	controltypepos varchar(255) DEFAULT '' NOT NULL,
	controlstreetviewpos varchar(255) DEFAULT '' NOT NULL,
	controlrotatepos varchar(255) DEFAULT '' NOT NULL,
	controlfullscreenpos varchar(255) DEFAULT '' NOT NULL,
);