<?php
namespace KITT3N\Kitt3nMap\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Oliver Merz <o.merz@kitt3n.de>
 * @author Georg Kathan <g.kathan@kitt3n.de>
 * @author Dominik Hilser <d.hilser@kitt3n.de>
 */
class ItemTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nMap\Domain\Model\Item
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KITT3N\Kitt3nMap\Domain\Model\Item();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTypeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getType()
        );
    }

    /**
     * @test
     */
    public function setTypeForIntSetsType()
    {
        $this->subject->setType(12);

        self::assertAttributeEquals(
            12,
            'type',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLatReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLat()
        );
    }

    /**
     * @test
     */
    public function setLatForStringSetsLat()
    {
        $this->subject->setLat('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lat',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLonReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLon()
        );
    }

    /**
     * @test
     */
    public function setLonForStringSetsLon()
    {
        $this->subject->setLon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIcon()
        );
    }

    /**
     * @test
     */
    public function setIconForStringSetsIcon()
    {
        $this->subject->setIcon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'icon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIcondefaultsReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getIcondefaults()
        );
    }

    /**
     * @test
     */
    public function setIcondefaultsForIntSetsIcondefaults()
    {
        $this->subject->setIcondefaults(12);

        self::assertAttributeEquals(
            12,
            'icondefaults',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconcolorReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIconcolor()
        );
    }

    /**
     * @test
     */
    public function setIconcolorForStringSetsIconcolor()
    {
        $this->subject->setIconcolor('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'iconcolor',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconwidthReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getIconwidth()
        );
    }

    /**
     * @test
     */
    public function setIconwidthForIntSetsIconwidth()
    {
        $this->subject->setIconwidth(12);

        self::assertAttributeEquals(
            12,
            'iconwidth',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconheightReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getIconheight()
        );
    }

    /**
     * @test
     */
    public function setIconheightForIntSetsIconheight()
    {
        $this->subject->setIconheight(12);

        self::assertAttributeEquals(
            12,
            'iconheight',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconoriginxReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getIconoriginx()
        );
    }

    /**
     * @test
     */
    public function setIconoriginxForIntSetsIconoriginx()
    {
        $this->subject->setIconoriginx(12);

        self::assertAttributeEquals(
            12,
            'iconoriginx',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconoriginyReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getIconoriginy()
        );
    }

    /**
     * @test
     */
    public function setIconoriginyForIntSetsIconoriginy()
    {
        $this->subject->setIconoriginy(12);

        self::assertAttributeEquals(
            12,
            'iconoriginy',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconfileReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getIconfile()
        );
    }

    /**
     * @test
     */
    public function setIconfileForFileReferenceSetsIconfile()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setIconfile($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'iconfile',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPathsReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getPaths()
        );
    }

    /**
     * @test
     */
    public function setPathsForFileReferenceSetsPaths()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setPaths($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'paths',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStrokecolorReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStrokecolor()
        );
    }

    /**
     * @test
     */
    public function setStrokecolorForStringSetsStrokecolor()
    {
        $this->subject->setStrokecolor('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'strokecolor',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStrokeopacityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStrokeopacity()
        );
    }

    /**
     * @test
     */
    public function setStrokeopacityForStringSetsStrokeopacity()
    {
        $this->subject->setStrokeopacity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'strokeopacity',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStrokewidthReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getStrokewidth()
        );
    }

    /**
     * @test
     */
    public function setStrokewidthForIntSetsStrokewidth()
    {
        $this->subject->setStrokewidth(12);

        self::assertAttributeEquals(
            12,
            'strokewidth',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFillcolorReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFillcolor()
        );
    }

    /**
     * @test
     */
    public function setFillcolorForStringSetsFillcolor()
    {
        $this->subject->setFillcolor('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fillcolor',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFillopacityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFillopacity()
        );
    }

    /**
     * @test
     */
    public function setFillopacityForStringSetsFillopacity()
    {
        $this->subject->setFillopacity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fillopacity',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getScaleReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getScale()
        );
    }

    /**
     * @test
     */
    public function setScaleForFloatSetsScale()
    {
        $this->subject->setScale(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'scale',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getWindowReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getWindow()
        );
    }

    /**
     * @test
     */
    public function setWindowForStringSetsWindow()
    {
        $this->subject->setWindow('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'window',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWindowimageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getWindowimage()
        );
    }

    /**
     * @test
     */
    public function setWindowimageForFileReferenceSetsWindowimage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setWindowimage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'windowimage',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWindowstateReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getWindowstate()
        );
    }

    /**
     * @test
     */
    public function setWindowstateForBoolSetsWindowstate()
    {
        $this->subject->setWindowstate(true);

        self::assertAttributeEquals(
            true,
            'windowstate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAnimationtypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAnimationtype()
        );
    }

    /**
     * @test
     */
    public function setAnimationtypeForStringSetsAnimationtype()
    {
        $this->subject->setAnimationtype('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'animationtype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAnimationdelayReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getAnimationdelay()
        );
    }

    /**
     * @test
     */
    public function setAnimationdelayForIntSetsAnimationdelay()
    {
        $this->subject->setAnimationdelay(12);

        self::assertAttributeEquals(
            12,
            'animationdelay',
            $this->subject
        );
    }
}
