<?php

namespace KITT3N\Kitt3nMap\Hooks\Previews;

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Database\ConnectionPool;

class MapPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    const EXTENSION_KEY = 'kitt3n_map';
    const DEFAULT_TRANSLATION_PATH = 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:';

    /**
     * Preprocesses the preview rendering of a content element of type "My new content element"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    )
    {

        if ($row['list_type'] !== 'kitt3nmap_kitt3nmaprender') {
            return;
        } else {

            // ConfigurationManager initilisieren
            $this->configurationManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

            // parse xml from flexform to array
            $ffXml = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($row['pi_flexform']);

            // get oContacts
            $sSelectedMap = $ffXml['data']['sDEF']['lDEF']['settings.sMap']['vDEF'];

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_kitt3nmap_domain_model_map');
            $statement = $queryBuilder
                ->select('name', 'width', 'widthunit', 'heightunit', 'height', 'fitbounds', 'lat', 'lon', 'zoom', 'minzoom', 'maxzoom', 'language', 'type', 'controls' ,'mc')
                ->from('tx_kitt3nmap_domain_model_map')
                ->where(
                    $queryBuilder->expr()->in('uid', $sSelectedMap)
                )
                ->execute();

            $aLabels = [];
            $aLabels[1]['label'] = 'Typ';
            $aLabels[2]['label'] = 'Breite';
            $aLabels[3]['label'] = 'Höhe';
            $aLabels[4]['label'] = 'Zentrum';
            $aLabels[5]['label'] = 'Zoom';
//            $aLabels[6]['label'] = 'Sprache';
            $aLabels[6]['label'] = 'Bedienelemente';
            $aLabels[7]['label'] = 'Marker Cluster';


            $aRow = $statement->fetch();
            $sRows = '';
            $j = 1;
            foreach ($aLabels as $aLabel){
                $sRows .= '<tr><th>' . $aLabel['label'] . '</th>';
//                $sLanguage = ($aRow['language'] == 'zh-CN' || $aRow['language'] == 'zh-TW' ? 'cn' : $aRow['language']);

                switch ($j){
                    case 1:
                        $sRows .= '<td><span> ' . $aRow['type'] . '</span></td>';
                        break;
                    case 2:
                        $sRows .= '<td><span><i class="fa fa-arrows-h"></i> ' . $aRow['width'] . ($aRow['widthunit'] == 0 ? '%' : 'px') . '</span></td>';
                        break;
                    case 3:
                        $sRows .= '<td><span><i class="fa fa-arrows-v"></i> ' . $aRow['height'] . ($aRow['heightunit'] == 0 ? '%' : 'px') . '</span></td>';
                        break;
                    case 4:
                        $sRows .= '<td><span> ' . ($aRow['fitbounds'] ? 'auto <i>(FitBounds)</i>' : ($aRow['lat'] && $aRow['lon'] ? 'Längengrad: ' . $aRow['lon'] . ', Breitengrad: ' . $aRow['lat'] : '-')) . '</span></td>';
                        break;
                    case 5:
                        $sRows .= '<td><span><span class="badge badge-warning" style="margin-right: 10px;"><span style="font-weight: normal;">Min:</span> ' . $aRow['minzoom'] . '</span><span class="badge badge-dark" style="margin-right: 10px;"><span style="font-weight: normal;">Standard:</span> ' . $aRow['zoom'] . '</span><span class="badge badge-warning"><span style="font-weight: normal;">Max:</span> ' . $aRow['maxzoom'] . '</span></span></td>';
                        break;
//                    case 6:
//                        $sRows .= '<td><span><span class="t3js-icon icon icon-size-small icon-state-default icon-flags-' . $sLanguage . '" data-identifier="flags-' . $sLanguage . '"><span class="icon-markup"><img src="/typo3/sysext/core/Resources/Public/Icons/Flags/' . strtoupper($sLanguage) . '.png" width="16" height="16"></span></span> ' . \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(self::DEFAULT_TRANSLATION_PATH . 'tx_kitt3nmap_domain_model_map.tca.column.language.option.' . $aRow['language'] , self::EXTENSION_KEY) . '</span></td>';
//                        break;
                    case 6:
                        $sRows .= '<td><span> ' . ($aRow['controls'] == 1 ? '<i class="fa fa-close"></i> ' : '') . \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(self::DEFAULT_TRANSLATION_PATH . 'tx_kitt3nmap_domain_model_map.tca.column.controls.option.' . $aRow['controls'] , self::EXTENSION_KEY) . '</span></td>';
                        break;
                    case 7:
                        $sRows .= '<td> ' . ($aRow['mc'] == 0 ? '<i class="fa fa-close"></i> ' . \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(self::DEFAULT_TRANSLATION_PATH . 'tx_kitt3nmap_domain_model_map.label.disabled' , self::EXTENSION_KEY) : '<i class="fa fa-check"></i> ' . \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(self::DEFAULT_TRANSLATION_PATH . 'tx_kitt3nmap_domain_model_map.label.enabled' , self::EXTENSION_KEY)) . '</td>';
                        break;
                }

                $sRows .= '</tr>';

                $j++;
            }


            // todo: possible featute backend preview of static image https://maps.googleapis.com/maps/api/staticmap?size=480x480&zoom=4&center=48.067539,8.463130&format=JPEG&maptype=terrain&language=de&key=AIzaSyB-QuYWeBtVZRDy3TqSc2owU9tygZgXp7E

            // preview html
            $sContentHtml = '            
            <div class="kitt3n_map--be">
                <strong>KITT3N Map (Google Map)</strong>
                <div style="margin: 5px 0 10px 0;">
                    <span class="t3js-icon icon icon-size-small icon-state-default icon-tcarecords-tx_kitt3nmap_domain_model_map-default" data-identifier="tcarecords-tx_kitt3nmap_domain_model_map-default" style="margin-right: 5px;">
	                    <span class="icon-markup">
                            <img src="/typo3conf/ext/kitt3n_map/Resources/Public/Icons/tx_kitt3nmap_domain_model_map.svg" width="16" height="16">
	                    </span>	
                    </span>
                    <span>' . $aRow['name'] . '</span>
                </div>                
                <table class="table table-condensed" style="padding: 20px 0; border: none;">
                    <tbody>            
                        '. $sRows .'                       
                    </tbody>
                </table>
            </div>            
            ';

            $itemContent = $sContentHtml;
            $drawItem = false;

        }
    }
}