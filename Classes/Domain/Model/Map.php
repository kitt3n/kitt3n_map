<?php
namespace KITT3N\Kitt3nMap\Domain\Model;


/***
 *
 * This file is part of the "kitt3n | Map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * Map
 */
class Map extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     * 
     * @var string
     */
    protected $name = '';

    /**
     * stylefile
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $stylefile = null;

    /**
     * width
     * 
     * @var string
     */
    protected $width = '';

    /**
     * widthunit
     * 
     * @var int
     */
    protected $widthunit = 0;

    /**
     * height
     * 
     * @var string
     */
    protected $height = '';

    /**
     * heightunit
     * 
     * @var int
     */
    protected $heightunit = 0;

    /**
     * fitbounds
     * 
     * @var bool
     */
    protected $fitbounds = false;

    /**
     * lat
     * 
     * @var string
     */
    protected $lat = '';

    /**
     * lon
     * 
     * @var string
     */
    protected $lon = '';

    /**
     * zoom
     * 
     * @var int
     */
    protected $zoom = 0;

    /**
     * minzoom
     * 
     * @var int
     */
    protected $minzoom = 0;

    /**
     * maxzoom
     * 
     * @var int
     */
    protected $maxzoom = 0;

    /**
     * language
     * 
     * @var string
     */
    protected $language = 0;

    /**
     * type
     * 
     * @var string
     */
    protected $type = 0;

    /**
     * tilt
     * 
     * @var int
     */
    protected $tilt = 0;

    /**
     * controls
     * 
     * @var int
     */
    protected $controls = '';

    /**
     * controlsize
     * 
     * @var string
     */
    protected $controlsize = 0;

    /**
     * controlzoom
     * 
     * @var bool
     */
    protected $controlzoom = false;

    /**
     * controlzoompos
     * 
     * @var string
     */
    protected $controlzoompos = 0;

    /**
     * controltype
     * 
     * @var bool
     */
    protected $controltype = false;

    /**
     * controltypepos
     * 
     * @var string
     */
    protected $controltypepos = 0;

    /**
     * controltypestyle
     * 
     * @var string
     */
    protected $controltypestyle = 0;

    /**
     * controlstreetview
     * 
     * @var bool
     */
    protected $controlstreetview = false;

    /**
     * controlstreetviewpos
     * 
     * @var string
     */
    protected $controlstreetviewpos = 0;

    /**
     * controlrotate
     * 
     * @var bool
     */
    protected $controlrotate = false;

    /**
     * controlrotatepos
     * 
     * @var string
     */
    protected $controlrotatepos = 0;

    /**
     * controlscale
     * 
     * @var bool
     */
    protected $controlscale = false;

    /**
     * controlfullscreen
     * 
     * @var bool
     */
    protected $controlfullscreen = false;

    /**
     * controlfullscreenpos
     * 
     * @var string
     */
    protected $controlfullscreenpos = 0;

    /**
     * panning
     * 
     * @var bool
     */
    protected $panning = false;

    /**
     * keyboardshortcuts
     * 
     * @var bool
     */
    protected $keyboardshortcuts = false;

    /**
     * doubleclickzoom
     * 
     * @var bool
     */
    protected $doubleclickzoom = false;

    /**
     * scrollzoom
     * 
     * @var bool
     */
    protected $scrollzoom = false;

    /**
     * clickablepoints
     * 
     * @var bool
     */
    protected $clickablepoints = false;

    /**
     * mc
     * 
     * @var bool
     */
    protected $mc = false;

    /**
     * mctype
     * 
     * @var int
     */
    protected $mctype = 0;

    /**
     * mcgridsize
     * 
     * @var int
     */
    protected $mcgridsize = 0;

    /**
     * mcmaxzoom
     * 
     * @var int
     */
    protected $mcmaxzoom = 0;

    /**
     * mczoomonclick
     * 
     * @var bool
     */
    protected $mczoomonclick = false;

    /**
     * mcaveragecenter
     * 
     * @var bool
     */
    protected $mcaveragecenter = false;

    /**
     * mcminclustersize
     * 
     * @var int
     */
    protected $mcminclustersize = 0;

    /**
     * mcstyles
     * 
     * @var string
     */
    protected $mcstyles = '';

    /**
     * item
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nMap\Domain\Model\Item>
     */
    protected $item = null;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->item = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the stylefile
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $stylefile
     */
    public function getStylefile()
    {
        return $this->stylefile;
    }

    /**
     * Sets the stylefile
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $stylefile
     * @return void
     */
    public function setStylefile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $stylefile)
    {
        $this->stylefile = $stylefile;
    }

    /**
     * Returns the width
     * 
     * @return string $width
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Sets the width
     * 
     * @param string $width
     * @return void
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * Returns the widthunit
     * 
     * @return int $widthunit
     */
    public function getWidthunit()
    {
        return $this->widthunit;
    }

    /**
     * Sets the widthunit
     * 
     * @param int $widthunit
     * @return void
     */
    public function setWidthunit($widthunit)
    {
        $this->widthunit = $widthunit;
    }

    /**
     * Returns the height
     * 
     * @return string $height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Sets the height
     * 
     * @param string $height
     * @return void
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * Returns the heightunit
     * 
     * @return int $heightunit
     */
    public function getHeightunit()
    {
        return $this->heightunit;
    }

    /**
     * Sets the heightunit
     * 
     * @param int $heightunit
     * @return void
     */
    public function setHeightunit($heightunit)
    {
        $this->heightunit = $heightunit;
    }

    /**
     * Returns the fitbounds
     * 
     * @return bool $fitbounds
     */
    public function getFitbounds()
    {
        return $this->fitbounds;
    }

    /**
     * Sets the fitbounds
     * 
     * @param bool $fitbounds
     * @return void
     */
    public function setFitbounds($fitbounds)
    {
        $this->fitbounds = $fitbounds;
    }

    /**
     * Returns the boolean state of fitbounds
     * 
     * @return bool
     */
    public function isFitbounds()
    {
        return $this->fitbounds;
    }

    /**
     * Returns the lat
     * 
     * @return string $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Sets the lat
     * 
     * @param string $lat
     * @return void
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * Returns the lon
     * 
     * @return string $lon
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Sets the lon
     * 
     * @param string $lon
     * @return void
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
    }

    /**
     * Returns the zoom
     * 
     * @return int $zoom
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * Sets the zoom
     * 
     * @param int $zoom
     * @return void
     */
    public function setZoom($zoom)
    {
        $this->zoom = $zoom;
    }

    /**
     * Returns the minzoom
     * 
     * @return int $minzoom
     */
    public function getMinzoom()
    {
        return $this->minzoom;
    }

    /**
     * Sets the minzoom
     * 
     * @param int $minzoom
     * @return void
     */
    public function setMinzoom($minzoom)
    {
        $this->minzoom = $minzoom;
    }

    /**
     * Returns the maxzoom
     * 
     * @return int $maxzoom
     */
    public function getMaxzoom()
    {
        return $this->maxzoom;
    }

    /**
     * Sets the maxzoom
     * 
     * @param int $maxzoom
     * @return void
     */
    public function setMaxzoom($maxzoom)
    {
        $this->maxzoom = $maxzoom;
    }

    /**
     * Returns the tilt
     * 
     * @return int $tilt
     */
    public function getTilt()
    {
        return $this->tilt;
    }

    /**
     * Sets the tilt
     * 
     * @param int $tilt
     * @return void
     */
    public function setTilt($tilt)
    {
        $this->tilt = $tilt;
    }

    /**
     * Returns the controlzoom
     * 
     * @return bool $controlzoom
     */
    public function getControlzoom()
    {
        return $this->controlzoom;
    }

    /**
     * Sets the controlzoom
     * 
     * @param bool $controlzoom
     * @return void
     */
    public function setControlzoom($controlzoom)
    {
        $this->controlzoom = $controlzoom;
    }

    /**
     * Returns the boolean state of controlzoom
     * 
     * @return bool
     */
    public function isControlzoom()
    {
        return $this->controlzoom;
    }

    /**
     * Returns the controltype
     * 
     * @return bool $controltype
     */
    public function getControltype()
    {
        return $this->controltype;
    }

    /**
     * Sets the controltype
     * 
     * @param bool $controltype
     * @return void
     */
    public function setControltype($controltype)
    {
        $this->controltype = $controltype;
    }

    /**
     * Returns the boolean state of controltype
     * 
     * @return bool
     */
    public function isControltype()
    {
        return $this->controltype;
    }

    /**
     * Returns the controlstreetview
     * 
     * @return bool $controlstreetview
     */
    public function getControlstreetview()
    {
        return $this->controlstreetview;
    }

    /**
     * Sets the controlstreetview
     * 
     * @param bool $controlstreetview
     * @return void
     */
    public function setControlstreetview($controlstreetview)
    {
        $this->controlstreetview = $controlstreetview;
    }

    /**
     * Returns the boolean state of controlstreetview
     * 
     * @return bool
     */
    public function isControlstreetview()
    {
        return $this->controlstreetview;
    }

    /**
     * Returns the controlrotate
     * 
     * @return bool $controlrotate
     */
    public function getControlrotate()
    {
        return $this->controlrotate;
    }

    /**
     * Sets the controlrotate
     * 
     * @param bool $controlrotate
     * @return void
     */
    public function setControlrotate($controlrotate)
    {
        $this->controlrotate = $controlrotate;
    }

    /**
     * Returns the boolean state of controlrotate
     * 
     * @return bool
     */
    public function isControlrotate()
    {
        return $this->controlrotate;
    }

    /**
     * Returns the controlscale
     * 
     * @return bool $controlscale
     */
    public function getControlscale()
    {
        return $this->controlscale;
    }

    /**
     * Sets the controlscale
     * 
     * @param bool $controlscale
     * @return void
     */
    public function setControlscale($controlscale)
    {
        $this->controlscale = $controlscale;
    }

    /**
     * Returns the boolean state of controlscale
     * 
     * @return bool
     */
    public function isControlscale()
    {
        return $this->controlscale;
    }

    /**
     * Returns the controlfullscreen
     * 
     * @return bool $controlfullscreen
     */
    public function getControlfullscreen()
    {
        return $this->controlfullscreen;
    }

    /**
     * Sets the controlfullscreen
     * 
     * @param bool $controlfullscreen
     * @return void
     */
    public function setControlfullscreen($controlfullscreen)
    {
        $this->controlfullscreen = $controlfullscreen;
    }

    /**
     * Returns the boolean state of controlfullscreen
     * 
     * @return bool
     */
    public function isControlfullscreen()
    {
        return $this->controlfullscreen;
    }

    /**
     * Returns the panning
     * 
     * @return bool $panning
     */
    public function getPanning()
    {
        return $this->panning;
    }

    /**
     * Sets the panning
     * 
     * @param bool $panning
     * @return void
     */
    public function setPanning($panning)
    {
        $this->panning = $panning;
    }

    /**
     * Returns the boolean state of panning
     * 
     * @return bool
     */
    public function isPanning()
    {
        return $this->panning;
    }

    /**
     * Returns the keyboardshortcuts
     * 
     * @return bool $keyboardshortcuts
     */
    public function getKeyboardshortcuts()
    {
        return $this->keyboardshortcuts;
    }

    /**
     * Sets the keyboardshortcuts
     * 
     * @param bool $keyboardshortcuts
     * @return void
     */
    public function setKeyboardshortcuts($keyboardshortcuts)
    {
        $this->keyboardshortcuts = $keyboardshortcuts;
    }

    /**
     * Returns the boolean state of keyboardshortcuts
     * 
     * @return bool
     */
    public function isKeyboardshortcuts()
    {
        return $this->keyboardshortcuts;
    }

    /**
     * Returns the doubleclickzoom
     * 
     * @return bool $doubleclickzoom
     */
    public function getDoubleclickzoom()
    {
        return $this->doubleclickzoom;
    }

    /**
     * Sets the doubleclickzoom
     * 
     * @param bool $doubleclickzoom
     * @return void
     */
    public function setDoubleclickzoom($doubleclickzoom)
    {
        $this->doubleclickzoom = $doubleclickzoom;
    }

    /**
     * Returns the boolean state of doubleclickzoom
     * 
     * @return bool
     */
    public function isDoubleclickzoom()
    {
        return $this->doubleclickzoom;
    }

    /**
     * Returns the scrollzoom
     * 
     * @return bool $scrollzoom
     */
    public function getScrollzoom()
    {
        return $this->scrollzoom;
    }

    /**
     * Sets the scrollzoom
     * 
     * @param bool $scrollzoom
     * @return void
     */
    public function setScrollzoom($scrollzoom)
    {
        $this->scrollzoom = $scrollzoom;
    }

    /**
     * Returns the boolean state of scrollzoom
     * 
     * @return bool
     */
    public function isScrollzoom()
    {
        return $this->scrollzoom;
    }

    /**
     * Returns the clickablepoints
     * 
     * @return bool $clickablepoints
     */
    public function getClickablepoints()
    {
        return $this->clickablepoints;
    }

    /**
     * Sets the clickablepoints
     * 
     * @param bool $clickablepoints
     * @return void
     */
    public function setClickablepoints($clickablepoints)
    {
        $this->clickablepoints = $clickablepoints;
    }

    /**
     * Returns the boolean state of clickablepoints
     * 
     * @return bool
     */
    public function isClickablepoints()
    {
        return $this->clickablepoints;
    }

    /**
     * Adds a Item
     * 
     * @param \KITT3N\Kitt3nMap\Domain\Model\Item $item
     * @return void
     */
    public function addItem(\KITT3N\Kitt3nMap\Domain\Model\Item $item)
    {
        $this->item->attach($item);
    }

    /**
     * Removes a Item
     * 
     * @param \KITT3N\Kitt3nMap\Domain\Model\Item $itemToRemove The Item to be removed
     * @return void
     */
    public function removeItem(\KITT3N\Kitt3nMap\Domain\Model\Item $itemToRemove)
    {
        $this->item->detach($itemToRemove);
    }

    /**
     * Returns the item
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nMap\Domain\Model\Item> $item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Sets the item
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nMap\Domain\Model\Item> $item
     * @return void
     */
    public function setItem(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $item)
    {
        $this->item = $item;
    }

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the language
     * 
     * @return string language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Sets the language
     * 
     * @param int $language
     * @return void
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * Returns the type
     * 
     * @return string type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     * 
     * @param int $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the controls
     * 
     * @return int controls
     */
    public function getControls()
    {
        return $this->controls;
    }

    /**
     * Sets the controls
     * 
     * @param string $controls
     * @return void
     */
    public function setControls($controls)
    {
        $this->controls = $controls;
    }

    /**
     * Returns the controlzoompos
     * 
     * @return string controlzoompos
     */
    public function getControlzoompos()
    {
        return $this->controlzoompos;
    }

    /**
     * Sets the controlzoompos
     * 
     * @param int $controlzoompos
     * @return void
     */
    public function setControlzoompos($controlzoompos)
    {
        $this->controlzoompos = $controlzoompos;
    }

    /**
     * Returns the controltypepos
     * 
     * @return string controltypepos
     */
    public function getControltypepos()
    {
        return $this->controltypepos;
    }

    /**
     * Sets the controltypepos
     * 
     * @param int $controltypepos
     * @return void
     */
    public function setControltypepos($controltypepos)
    {
        $this->controltypepos = $controltypepos;
    }

    /**
     * Returns the controltypestyle
     * 
     * @return string controltypestyle
     */
    public function getControltypestyle()
    {
        return $this->controltypestyle;
    }

    /**
     * Sets the controltypestyle
     * 
     * @param int $controltypestyle
     * @return void
     */
    public function setControltypestyle($controltypestyle)
    {
        $this->controltypestyle = $controltypestyle;
    }

    /**
     * Returns the controlstreetviewpos
     * 
     * @return string controlstreetviewpos
     */
    public function getControlstreetviewpos()
    {
        return $this->controlstreetviewpos;
    }

    /**
     * Sets the controlstreetviewpos
     * 
     * @param int $controlstreetviewpos
     * @return void
     */
    public function setControlstreetviewpos($controlstreetviewpos)
    {
        $this->controlstreetviewpos = $controlstreetviewpos;
    }

    /**
     * Returns the controlrotatepos
     * 
     * @return string controlrotatepos
     */
    public function getControlrotatepos()
    {
        return $this->controlrotatepos;
    }

    /**
     * Sets the controlrotatepos
     * 
     * @param int $controlrotatepos
     * @return void
     */
    public function setControlrotatepos($controlrotatepos)
    {
        $this->controlrotatepos = $controlrotatepos;
    }

    /**
     * Returns the controlfullscreenpos
     * 
     * @return string controlfullscreenpos
     */
    public function getControlfullscreenpos()
    {
        return $this->controlfullscreenpos;
    }

    /**
     * Sets the controlfullscreenpos
     * 
     * @param int $controlfullscreenpos
     * @return void
     */
    public function setControlfullscreenpos($controlfullscreenpos)
    {
        $this->controlfullscreenpos = $controlfullscreenpos;
    }

    /**
     * Returns the controlsize
     * 
     * @return string controlsize
     */
    public function getControlsize()
    {
        return $this->controlsize;
    }

    /**
     * Sets the controlsize
     * 
     * @param int $controlsize
     * @return void
     */
    public function setControlsize($controlsize)
    {
        $this->controlsize = $controlsize;
    }

    /**
     * Returns the mc
     * 
     * @return bool $mc
     */
    public function getMc()
    {
        return $this->mc;
    }

    /**
     * Sets the mc
     * 
     * @param bool $mc
     * @return void
     */
    public function setMc($mc)
    {
        $this->mc = $mc;
    }

    /**
     * Returns the boolean state of mc
     * 
     * @return bool
     */
    public function isMc()
    {
        return $this->mc;
    }

    /**
     * Returns the mctype
     * 
     * @return int $mctype
     */
    public function getMctype()
    {
        return $this->mctype;
    }

    /**
     * Sets the mctype
     * 
     * @param int $mctype
     * @return void
     */
    public function setMctype($mctype)
    {
        $this->mctype = $mctype;
    }

    /**
     * Returns the mcgridsize
     * 
     * @return int $mcgridsize
     */
    public function getMcgridsize()
    {
        return $this->mcgridsize;
    }

    /**
     * Sets the mcgridsize
     * 
     * @param int $mcgridsize
     * @return void
     */
    public function setMcgridsize($mcgridsize)
    {
        $this->mcgridsize = $mcgridsize;
    }

    /**
     * Returns the mczoomonclick
     * 
     * @return bool $mczoomonclick
     */
    public function getMczoomonclick()
    {
        return $this->mczoomonclick;
    }

    /**
     * Sets the mczoomonclick
     * 
     * @param bool $mczoomonclick
     * @return void
     */
    public function setMczoomonclick($mczoomonclick)
    {
        $this->mczoomonclick = $mczoomonclick;
    }

    /**
     * Returns the boolean state of mczoomonclick
     * 
     * @return bool
     */
    public function isMczoomonclick()
    {
        return $this->mczoomonclick;
    }

    /**
     * Returns the mcaveragecenter
     * 
     * @return bool $mcaveragecenter
     */
    public function getMcaveragecenter()
    {
        return $this->mcaveragecenter;
    }

    /**
     * Sets the mcaveragecenter
     * 
     * @param bool $mcaveragecenter
     * @return void
     */
    public function setMcaveragecenter($mcaveragecenter)
    {
        $this->mcaveragecenter = $mcaveragecenter;
    }

    /**
     * Returns the boolean state of mcaveragecenter
     * 
     * @return bool
     */
    public function isMcaveragecenter()
    {
        return $this->mcaveragecenter;
    }

    /**
     * Returns the mcminclustersize
     * 
     * @return int $mcminclustersize
     */
    public function getMcminclustersize()
    {
        return $this->mcminclustersize;
    }

    /**
     * Sets the mcminclustersize
     * 
     * @param int $mcminclustersize
     * @return void
     */
    public function setMcminclustersize($mcminclustersize)
    {
        $this->mcminclustersize = $mcminclustersize;
    }

    /**
     * Returns the mcstyles
     * 
     * @return string $mcstyles
     */
    public function getMcstyles()
    {
        return $this->mcstyles;
    }

    /**
     * Sets the mcstyles
     * 
     * @param string $mcstyles
     * @return void
     */
    public function setMcstyles($mcstyles)
    {
        $this->mcstyles = $mcstyles;
    }

    /**
     * Returns the mcmaxzoom
     * 
     * @return int mcmaxzoom
     */
    public function getMcmaxzoom()
    {
        return $this->mcmaxzoom;
    }

    /**
     * Sets the mcmaxzoom
     * 
     * @param int $mcmaxzoom
     * @return void
     */
    public function setMcmaxzoom($mcmaxzoom)
    {
        $this->mcmaxzoom = $mcmaxzoom;
    }
}
