<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
   'kitt3n_map',
   'tx_kitt3nmap_domain_model_map'
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$sModel = basename(__FILE__, '.php');

// Icon
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_map/Resources/Public/Icons/' . $sModel . '.svg';

// Backend labels
$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'type';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'name';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

// $GLOBALS['TCA'][$sModel]['columns']['streets']['config']['foreign_sortby'] = 'name';

// Tabs and palettes
$GLOBALS['TCA'][$sModel]['types'][1] = [
    'showitem' => '
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.tab.style,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.name;name,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.type;type,

            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.style;style,
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.tab.size,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.size;size, 
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.alignment;alignment, 
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.zoom;zoom,               
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.tab.items,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.items;items,
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.tab.controls,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.controls;controls,
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.tab.behavior,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.behavior;behavior,
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.tab.markerclusterer,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.palette.markerclusterer;markerclusterer,        
                       
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;systemLanguage,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;paletteHidden,
        --div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories
    '
];
// Palettes | ToDo: "tilt" is missing
$GLOBALS['TCA'][$sModel]['palettes'] = [
    'name' => [
        'showitem' => 'name'
    ],
    'type' => [
        'showitem' => 'type'
    ],
//    'lang' => [
//        'showitem' => 'language'
//    ],
    'style' => [
        'showitem' => 'stylefile'
    ],

    'size' => [
        'showitem' => 'width, widthunit,--linebreak--, height, heightunit'
    ],
    'alignment' => [
        'showitem' => 'fitbounds,--linebreak--, lat, lon'
    ],
    'zoom' => [
        'showitem' => 'zoom,--linebreak--, minzoom,--linebreak--, maxzoom'
    ],

    'controls' => [
        'showitem' => 'controls,--linebreak--, controlsize,--linebreak--, controlzoom,--linebreak--, controlzoompos,--linebreak--, controltype,--linebreak--, controltypepos,--linebreak--, controltypestyle,--linebreak--, controlstreetview,--linebreak--, controlstreetviewpos,--linebreak--, controlrotate,--linebreak--, controlrotatepos,--linebreak--, controlscale,--linebreak--, controlfullscreen,--linebreak--, controlfullscreenpos'
    ],
    'behavior' => [
        'showitem' => 'panning,--linebreak--, keyboardshortcuts,--linebreak--, doubleclickzoom,--linebreak--, scrollzoom,--linebreak--, clickablepoints'
    ],
    'markerclusterer' => [
        'showitem' => 'mc,--linebreak--, mctype,--linebreak--, mcgridsize,--linebreak--, mcmaxzoom,--linebreak--, mczoomonclick,--linebreak--, mcaveragecenter,--linebreak--, mcminclustersize,--linebreak--, mcstyles'
    ],

    'items' => [
        'showitem' => 'item'
    ],

    'paletteHidden' => [
        'showitem' => '
                hidden, starttime, endtime
            ',
    ],
    'systemLanguage' => ['showitem' => 'sys_language_uid, l10n_parent'],
];



// Column specifications
//$GLOBALS['TCA'][$sModel]['columns']['name'] = [
//    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.zoom',
//    ...
//];



$GLOBALS['TCA'][$sModel]['columns']['stylefile'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.stylefile',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.stylefile',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'stylefile',
        [
            'appearance' => [
                'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
            ],
            'foreign_types' => [
                '0' => [
                    'showitem' => '
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                    'showitem' => '
                            --palette--;;filePalette'
                ],
//                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
//                    'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//                ],
//                \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
//                    'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//                ],
//                \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
//                    'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ]
            ],
            'maxitems' => 1
        ],
        'json'
    ),
];

$GLOBALS['TCA'][$sModel]['columns']['width'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.width',
    'config' => [
        'type' => 'input',
        'size' => 10,
        'eval' => 'trim'
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['widthunit'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.unit',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['%', 0],
            ['px', 1],
        ],
        'size' => 1,
        'maxitems' => 1,
        'eval' => '',
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['height'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.height',
    'config' => [
        'type' => 'input',
        'size' => 10,
        'eval' => 'trim'
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['heightunit'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.unit',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['%', 0],
            ['px', 1],
        ],
        'size' => 1,
        'maxitems' => 1,
        'eval' => '',
        'default' => 1,
    ],
];

// ToDo: Wenn TRUE > hide lat & lon
$GLOBALS['TCA'][$sModel]['columns']['fitbounds'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.fitbounds',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.fitbounds',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
    ],
    'onChange' => 'reload'
];
$GLOBALS['TCA'][$sModel]['columns']['lat'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.lat',
    'config' => [
        'type' => 'input',
        'size' => 10,
        'eval' => 'trim'
    ],
    'displayCond' => 'FIELD:fitbounds:=:FALSE',
];
$GLOBALS['TCA'][$sModel]['columns']['lon'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.lon',
    'config' => [
        'type' => 'input',
        'size' => 10,
        'eval' => 'trim'
    ],
    'displayCond' => 'FIELD:fitbounds:=:FALSE',
];

$GLOBALS['TCA'][$sModel]['columns']['zoom'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.zoom',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.zoom',
    'config' => [
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim,int',
        'range' => [
            'lower' => 0,
            'upper' => 20,
        ],
        'default' => 0,
        'slider' => [
            'step' => 1,
            'width' => 200,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['minzoom'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.minzoom',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.minzoom',
    'config' => [
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim,int',
        'range' => [
            'lower' => 0,
            'upper' => 20,
        ],
        'default' => 0,
        'slider' => [
            'step' => 1,
            'width' => 200,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['maxzoom'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.maxzoom',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.maxzoom',
    'config' => [
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim,int',
        'range' => [
            'lower' => 0,
            'upper' => 20,
        ],
        'default' => 20,
        'slider' => [
            'step' => 1,
            'width' => 200,
        ],
    ],
];

// ToDo: Translate item labels + when it works, add more languages
$GLOBALS['TCA'][$sModel]['columns']['language'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.language',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.en', 'en', 'EXT:core/Resources/Public/Icons/Flags/GB.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.de', 'de', 'EXT:core/Resources/Public/Icons/Flags/DE.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.es', 'es', 'EXT:core/Resources/Public/Icons/Flags/ES.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.fr', 'fr', 'EXT:core/Resources/Public/Icons/Flags/FR.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.it', 'it', 'EXT:core/Resources/Public/Icons/Flags/IT.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.pl', 'pl', 'EXT:core/Resources/Public/Icons/Flags/PL.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.ru', 'ru', 'EXT:core/Resources/Public/Icons/Flags/RU.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.zh_cn', 'zh-CN', 'EXT:core/Resources/Public/Icons/Flags/CN.png'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.language.option.zh_tw', 'zh-TW', 'EXT:core/Resources/Public/Icons/Flags/CN.png'],
        ],
        'default' => 'en',
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['type'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.type',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.type',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.type.option.roadmap', 'Roadmap'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.type.option.satellite', 'Satellite'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.type.option.terrain', 'Terrain'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.type.option.hybrid', 'Hybrid'],
        ],
        'default' => 'Roadmap',
    ],
];

// ToDo: Tilt is missing!

// ToDo: Wenn 2 > show additional settings
$GLOBALS['TCA'][$sModel]['columns']['controls'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controls',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controls',
    'config' => [
        'type' => 'radio',
        'items' => [
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controls.option.0', 0],
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controls.option.1', 1],
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controls.option.2', 2],
        ],
        'default' => 0
    ],
    'onChange' => 'reload'
];

$GLOBALS['TCA'][$sModel]['columns']['controlsize'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlsize',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlsize',
    'config' => [
        'type' => 'input',
        'size' => 10,
        'eval' => 'int,trim'
    ],
    'displayCond' => 'FIELD:controls:=:2',
];

$GLOBALS['TCA'][$sModel]['columns']['controlzoom'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlzoom',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlzoom',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
    ],
    'onChange' => 'reload',
    'displayCond' => 'FIELD:controls:=:2',
];
$GLOBALS['TCA'][$sModel]['columns']['controlzoompos'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlzoompos',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlzoompos',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_center', 'TOP_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_left', 'TOP_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_right', 'TOP_RIGHT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_top', 'LEFT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_top', 'RIGHT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_center', 'LEFT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_center', 'RIGHT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_bottom', 'LEFT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_bottom', 'RIGHT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_center', 'BOTTOM_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_left', 'BOTTOM_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_right', 'BOTTOM_RIGHT', ''],
        ],
        'default' => 'RIGHT_BOTTOM',
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:controls:=:2',
            'FIELD:controlzoom:REQ:true',
        ],
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['controltype'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controltype',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controltype',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
    ],
    'onChange' => 'reload',
    'displayCond' => 'FIELD:controls:=:2',
];
$GLOBALS['TCA'][$sModel]['columns']['controltypepos'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controltypepos',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controltypepos',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_center', 'TOP_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_left', 'TOP_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_right', 'TOP_RIGHT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_top', 'LEFT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_top', 'RIGHT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_center', 'LEFT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_center', 'RIGHT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_bottom', 'LEFT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_bottom', 'RIGHT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_center', 'BOTTOM_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_left', 'BOTTOM_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_right', 'BOTTOM_RIGHT', ''],
        ],
        'default' => 'TOP_LEFT',
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:controls:=:2',
            'FIELD:controltype:REQ:true',
        ],
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['controltypestyle'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controltypestyle',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controltypestyle',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controltypestyle.option.default', 'DEFAULT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controltypestyle.option.horizontal', 'HORIZONTAL_BAR', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controltypestyle.option.dropdown', 'DROPDOWN_MENU', '']
        ],
        'default' => 'DEFAULT',
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:controls:=:2',
            'FIELD:controltype:REQ:true',
        ],
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['controlstreetview'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlstreetview',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlstreetview',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
    ],
    'onChange' => 'reload',
    'displayCond' => 'FIELD:controls:=:2',
];
$GLOBALS['TCA'][$sModel]['columns']['controlstreetviewpos'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlstreetviewpos',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlstreetviewpos',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_center', 'TOP_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_left', 'TOP_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_right', 'TOP_RIGHT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_top', 'LEFT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_top', 'RIGHT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_center', 'LEFT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_center', 'RIGHT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_bottom', 'LEFT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_bottom', 'RIGHT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_center', 'BOTTOM_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_left', 'BOTTOM_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_right', 'BOTTOM_RIGHT', ''],
        ],
        'default' => 'RIGHT_BOTTOM',
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:controls:=:2',
            'FIELD:controlstreetview:REQ:true',
        ],
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['controlrotate'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlrotate',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlrotate',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
    ],
    'onChange' => 'reload',
    'displayCond' => 'FIELD:controls:=:2',
];
$GLOBALS['TCA'][$sModel]['columns']['controlrotatepos'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlrotatepos',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlrotatepos',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_center', 'TOP_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_left', 'TOP_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_right', 'TOP_RIGHT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_top', 'LEFT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_top', 'RIGHT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_center', 'LEFT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_center', 'RIGHT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_bottom', 'LEFT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_bottom', 'RIGHT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_center', 'BOTTOM_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_left', 'BOTTOM_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_right', 'BOTTOM_RIGHT', ''],
        ],
        'default' => 'RIGHT_BOTTOM',
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:controls:=:2',
            'FIELD:controlrotate:REQ:true',
        ],
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['controlscale'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlscale',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlscale',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
    ],
    'displayCond' => 'FIELD:controls:=:2',
];

$GLOBALS['TCA'][$sModel]['columns']['controlfullscreen'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlfullscreen',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlfullscreen',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
    ],
    'onChange' => 'reload',
    'displayCond' => 'FIELD:controls:=:2',
];
$GLOBALS['TCA'][$sModel]['columns']['controlfullscreenpos'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.controlfullscreenpos',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.controlfullscreenpos',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_center', 'TOP_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_left', 'TOP_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.top_right', 'TOP_RIGHT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_top', 'LEFT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_top', 'RIGHT_TOP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_center', 'LEFT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_center', 'RIGHT_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.left_bottom', 'LEFT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.right_bottom', 'RIGHT_BOTTOM', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_center', 'BOTTOM_CENTER', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_left', 'BOTTOM_LEFT', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.option.bottom_right', 'BOTTOM_RIGHT', ''],
        ],
        'default' => 'TOP_RIGHT',
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:controls:=:2',
            'FIELD:controlfullscreen:REQ:true',
        ],
    ]
];

// BEHAVIOR
$GLOBALS['TCA'][$sModel]['columns']['panning'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.panning',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.panning',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
        'default' => 0,
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['keyboardshortcuts'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.keyboardshortcuts',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.keyboardshortcuts',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
        'default' => 0,
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['doubleclickzoom'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.doubleclickzoom',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.doubleclickzoom',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
        'default' => 0,
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['scrollzoom'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.scrollzoom',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.scrollzoom',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
        'default' => 0,
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['clickablepoints'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.clickablepoints',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.clickablepoints',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
        'default' => 0,
    ]
];



$GLOBALS['TCA'][$sModel]['ctrl']['requestUpdate'] = 'controls';
$GLOBALS['TCA'][$sModel]['columns']['controls']['onchange'] = 'reload';

$GLOBALS['TCA'][$sModel]['columns']['item']['label'] = 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.item';
$GLOBALS['TCA'][$sModel]['columns']['item']['description'] = 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.item';
$GLOBALS['TCA'][$sModel]['columns']['item']['config']['foreign_table_where'] = 'AND tx_kitt3nmap_domain_model_item.deleted = 0 AND tx_kitt3nmap_domain_model_item.hidden = 0';




$GLOBALS['TCA'][$sModel]['columns']['mc'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mc',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mc',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
        'default' => 0,
    ],
    'onChange' => 'reload'
];
$GLOBALS['TCA'][$sModel]['columns']['mctype'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mctype',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mctype',
    'config' => [
        'type' => 'radio',
        'items' => [
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mctype.option.0', 0],
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mctype.option.1', 1],
        ],
        'default' => 0
    ],
    'displayCond' => 'FIELD:mc:=:1',
    'onChange' => 'reload'
];
$GLOBALS['TCA'][$sModel]['columns']['mcgridsize'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mcgridsize',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mcgridsize',
    'config' => [
        'type' => 'input',
        'size' => 10,
        'eval' => 'int,trim'
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:mc:=:1',
            'FIELD:mctype:=:1',
        ],
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['mcmaxzoom'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mcmaxzoom',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mcmaxzoom',
    'config' => [
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim,int',
        'range' => [
            'lower' => 0,
            'upper' => 20,
        ],
        'default' => 0,
        'slider' => [
            'step' => 1,
            'width' => 200,
        ],
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:mc:=:1',
            'FIELD:mctype:=:1',
        ],
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['mczoomonclick'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mczoomonclick',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mczoomonclick',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
        'default' => 0,
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:mc:=:1',
            'FIELD:mctype:=:1',
        ],
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['mcaveragecenter'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mcaveragecenter',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mcaveragecenter',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            [
                0 => '',
                1 => '',
                'labelChecked' => 'Enabled',
                'labelUnchecked' => 'Disabled',
            ]
        ],
        'default' => 0,
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:mc:=:1',
            'FIELD:mctype:=:1',
        ],
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['mcminclustersize'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mcminclustersize',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mcminclustersize',
    'config' => [
        'type' => 'input',
        'size' => 10,
        'eval' => 'int,trim'
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:mc:=:1',
            'FIELD:mctype:=:1',
        ],
    ]
];
// An object that has style properties.
$GLOBALS['TCA'][$sModel]['columns']['mcstyles'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.mcstyles',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_map.tca.column.description.mcstyles',
    'config' => [
        'type' => 'text',
        'cols' => 30,
        'rows' => 100
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:mc:=:1',
            'FIELD:mctype:=:1',
        ],
    ]
];