<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
   'kitt3n_map',
   'tx_kitt3nmap_domain_model_item'
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$sModel = basename(__FILE__, '.php');

// Icon
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_map/Resources/Public/Icons/' . $sModel . '.svg';


$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'type';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'name';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

// ToDo: Labels, descriptions + Translations

// Tabs and palettes
$GLOBALS['TCA'][$sModel]['types'][1] = [
    'showitem' => '
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.tab.type,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.type;type,
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.tab.coordinates,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.coordinates;coordinates,
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.tab.infowindow,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.infowindow;infowindow,
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.tab.styles,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.markertype;markertype,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.form;form,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.filling;filling,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.frame;frame,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.scaling;scaling,            
        --div--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.tab.animation,
            --palette--;LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.palette.animation;animation,

        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;systemLanguage,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;paletteHidden,
        --div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories
    '
];
// Palettes
$GLOBALS['TCA'][$sModel]['palettes'] = [
    'type' => [
        'showitem' => 'name,--linebreak--, type,'
    ],
    'coordinates' => [
        'showitem' => 'lat, lon, paths'
    ],
    'infowindow' => [
        'showitem' => 'windowstate, --linebreak--, window, windowimage'
    ],
    'markertype' => [
        'showitem' => 'icon'
    ],
    'form' => [
        'showitem' => 'icondefaults, iconfile, --linebreak--, iconwidth, iconheight, --linebreak--, iconoriginx, iconoriginy'
    ],
    'filling' => [
        'showitem' => 'fillcolor, --linebreak--, fillopacity'
    ],
    'frame' => [
        'showitem' => 'strokecolor, --linebreak--, strokeopacity, --linebreak--, strokewidth'
    ],
    'scaling' => [
        'showitem' => 'scale'
    ],
    'animation' => [
        'showitem' => 'animationtype, --linebreak--, animationdelay'
    ],

    'paletteHidden' => [
        'showitem' => 'hidden, starttime, endtime',
    ],
    'systemLanguage' => [
        'showitem' => 'sys_language_uid, l10n_parent'
    ],
];







// Type | Marker or Polygon
$GLOBALS['TCA'][$sModel]['columns']['type'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.type',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.type.option.0', 0, ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.type.option.1', 1, ''],
        ],
        'default' => 0,
    ],
    'onChange' => 'reload'
];

// Lat & Lon
$GLOBALS['TCA'][$sModel]['columns']['lat'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.lat',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim,required'
    ],
    'displayCond' => 'FIELD:type:=:0',
];
$GLOBALS['TCA'][$sModel]['columns']['lon'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.lon',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim,required'
    ],
    'displayCond' => 'FIELD:type:=:0',
];

$GLOBALS['TCA'][$sModel]['columns']['windowstate'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.windowstate',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.description.windowstate',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
        'default' => 0,
    ],
    'displayCond' => 'FIELD:type:=:0',
];
$GLOBALS['TCA'][$sModel]['columns']['window'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.window',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.description.window',
    'config' => [
        'type' => 'text',
        'enableRichtext' => true,
        'richtextConfiguration' => 'default',
        'fieldControl' => [
            'fullScreenRichtext' => [
                'disabled' => false,
            ],
        ],
        'cols' => 40,
        'rows' => 15,
        'eval' => 'trim',
    ],
    'displayCond' => 'FIELD:type:=:0',
];
$GLOBALS['TCA'][$sModel]['columns']['windowimage'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.windowimage',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.description.windowimage',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'windowimage',
        [
            'appearance' => [
                'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
            ],
            'foreign_types' => [
                '0' => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ]
            ],
            'maxitems' => 1
        ],
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
    ),
    'displayCond' => 'FIELD:type:=:0',
];

// Icon Stuff
$GLOBALS['TCA'][$sModel]['columns']['icon'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icon',
    'config' => [
        'type' => 'radio',
        'items' => [
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icon.option.0', 0],
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icon.option.1', 1],
            [ 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icon.option.2', 2],
        ],
        'default' => 0
    ],
    'displayCond' => 'FIELD:type:=:0',
    'onChange' => 'reload'
];
$GLOBALS['TCA'][$sModel]['columns']['icondefaults'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.0', 0, ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.1', 1, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/pin_fill.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.2', 2, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/pin_stroke.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.3', 3, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/pin_stroke-dot.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.4', 4, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/pin_fill-hole.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.5', 5, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/pin_stroke-hole.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.6', 6, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/shield_fill.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.7', 7, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/shield_stroke.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.8', 8, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/flag_fill.svg'],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.icondefaults.option.9', 9, 'EXT:kitt3n_map/Resources/Public/Icons/SVGs/flag_stroke.svg'],
        ],
        'size' => 1,
        'maxitems' => 1,
        'eval' => ''
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:icon:=:1',
        ],
    ],
];
// Size
$GLOBALS['TCA'][$sModel]['columns']['scale'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.scale',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:icon:=:1',
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['iconwidth'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.iconwidth',
    'config' => [
        'type' => 'input',
        'size' => 4,
        'eval' => 'int,required'
    ],
    'default' => '32',
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:icon:=:2',
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['iconheight'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.iconheight',
    'config' => [
        'type' => 'input',
        'size' => 4,
        'eval' => 'int,required'
    ],
    'default' => '32',
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:icon:=:2',
        ],
    ],
];
// Origin
$GLOBALS['TCA'][$sModel]['columns']['iconoriginx'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.iconoriginx',
    'description' => 'To detect anchor coordinates check out: https://www.mobilefish.com/services/record_mouse_coordinates/record_mouse_coordinates.php',
    'config' => [
        'type' => 'input',
        'size' => 4,
        'eval' => 'int,required'
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:icon:=:2',
        ]
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['iconoriginy'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.iconoriginy',
    'description' => 'To detect anchor coordinates check out: https://www.mobilefish.com/services/record_mouse_coordinates/record_mouse_coordinates.php',
    'config' => [
        'type' => 'input',
        'size' => 4,
        'eval' => 'int,required'
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:icon:=:2',
        ]
    ]
];
// File
$GLOBALS['TCA'][$sModel]['columns']['iconfile'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.iconfile',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'iconfile',
        [
            'appearance' => [
                'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
            ],
            'foreign_types' => [
                '0' => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ]
            ],
            'maxitems' => 1
        ],
        'svg,png'
    ),
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:icon:=:2',
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['iconfile']['config']['eval'] = 'required';


// Paths Stuff
$GLOBALS['TCA'][$sModel]['columns']['paths'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.paths',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.description.paths',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'paths',
        [
            'appearance' => [
                'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
            ],
            'foreign_types' => [
                '0' => [
                    'showitem' => '
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                    'showitem' => '
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ]
            ],
            'maxitems' => 1
        ],
        'json'
    ),
    'displayCond' => 'FIELD:type:=:1',
];
$GLOBALS['TCA'][$sModel]['columns']['strokecolor'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.strokecolor',
    'config' => [
        'type' => 'input',
        'renderType' => 'colorpicker',
        'size' => 10,
    ],
    'displayCond' => [
        'OR' => [
            'FIELD:type:=:1',
            'AND' => [
                'FIELD:type:=:0',
                'FIELD:icon:=:1',
            ]
        ]
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['strokeopacity'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.strokeopacity',
    'config' => [
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim,double',
        'range' => [
            'lower' => 0,
            'upper' => 1,
        ],
        'default' => 0,
        'slider' => [
            'step' => 0.1,
            'width' => 200,
        ],
    ],
    'displayCond' => [
        'OR' => [
            'FIELD:type:=:1',
            'AND' => [
                'FIELD:type:=:0',
                'FIELD:icon:=:1',
            ]
        ]
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['strokewidth'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.strokewidth',
    'config' => [
        'type' => 'input',
        'size' => 5,
        'eval' => 'int',
        'range' => [
            'lower' => 0,
            'upper' => 5,
        ],
        'default' => 0,
        'slider' => [
            'step' => 1,
            'width' => 200,
        ],
    ],
    'displayCond' => [
        'OR' => [
            'FIELD:type:=:1',
            'AND' => [
                'FIELD:type:=:0',
                'FIELD:icon:=:1',
            ]
        ]
    ]
];
$GLOBALS['TCA'][$sModel]['columns']['fillcolor'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.fillcolor',
    'config' => [
        'type' => 'input',
        'renderType' => 'colorpicker',
        'size' => 10,
    ],
    'displayCond' => [
        'OR' => [
            'FIELD:type:=:1',
            'AND' => [
                'FIELD:type:=:0',
                'FIELD:icon:=:1',
            ]
        ]
    ]

];
$GLOBALS['TCA'][$sModel]['columns']['fillopacity'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.fillopacity',
    'config' => [
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim,double',
        'range' => [
            'lower' => 0,
            'upper' => 1,
        ],
        'default' => 1,
        'slider' => [
            'step' => 0.1,
            'width' => 200,
        ],
    ],
    'displayCond' => [
        'OR' => [
            'FIELD:type:=:1',
            'AND' => [
                'FIELD:type:=:0',
                'FIELD:icon:=:1',
            ]
        ]
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['animationtype'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.animationtype',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.description.animationtype',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.animationtype.option.none', '', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.animationtype.option.drop', 'DROP', ''],
            ['LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.animationtype.option.bounce', 'BOUNCE', '']
        ],
        'default' => '',
    ],
    'onChange' => 'reload',
    'displayCond' => 'FIELD:type:=:0',
];
$GLOBALS['TCA'][$sModel]['columns']['animationdelay'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.animationdelay',
    'description' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.tca.column.description.animationdelay',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
    'displayCond' => [
        'AND' => [
            'FIELD:type:=:0',
            'FIELD:animationtype:!=:'
        ],
    ],
];