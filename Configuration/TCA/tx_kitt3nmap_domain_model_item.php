<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item',
        'label' => 'type',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'name,lat,lon,icon,iconcolor,strokecolor,strokeopacity,fillcolor,fillopacity,window,animationtype',
        'iconfile' => 'EXT:kitt3n_map/Resources/Public/Icons/tx_kitt3nmap_domain_model_item.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type, name, lat, lon, icon, icondefaults, iconcolor, iconwidth, iconheight, iconoriginx, iconoriginy, iconfile, paths, strokecolor, strokeopacity, strokewidth, fillcolor, fillopacity, scale, window, windowimage, windowstate, animationtype, animationdelay',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type, name, lat, lon, icon, icondefaults, iconcolor, iconwidth, iconheight, iconoriginx, iconoriginy, iconfile, paths, strokecolor, strokeopacity, strokewidth, fillcolor, fillopacity, scale, window, windowimage, windowstate, animationtype, animationdelay, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kitt3nmap_domain_model_item',
                'foreign_table_where' => 'AND {#tx_kitt3nmap_domain_model_item}.{#pid}=###CURRENT_PID### AND {#tx_kitt3nmap_domain_model_item}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lat' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.lat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lon' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.lon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'icon' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.icon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'icondefaults' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.icondefaults',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'iconcolor' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.iconcolor',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'iconwidth' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.iconwidth',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'iconheight' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.iconheight',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'iconoriginx' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.iconoriginx',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'iconoriginy' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.iconoriginy',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'iconfile' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.iconfile',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'iconfile',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                'svg,png'
            ),
        ],
        'paths' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.paths',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'paths',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1,
                    'minitems' => 1
                ],
                'json'
            ),
        ],
        'strokecolor' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.strokecolor',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'strokeopacity' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.strokeopacity',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'strokewidth' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.strokewidth',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'fillcolor' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.fillcolor',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'fillopacity' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.fillopacity',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'scale' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.scale',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2'
            ]
        ],
        'window' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.window',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            
        ],
        'windowimage' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.windowimage',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'windowimage',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'windowstate' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.windowstate',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'animationtype' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.animationtype',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'animationdelay' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_item.animationdelay',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
    
    ],
];
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder